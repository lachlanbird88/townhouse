<?php

Route::group(['prefix' => 'profile'], function () {
    Route::get('/', 'ProfileController@edit')->name('profile.edit'); // show profile edit form
    Route::patch('/', 'ProfileController@update')->name('profile.update'); // edit profile
});
